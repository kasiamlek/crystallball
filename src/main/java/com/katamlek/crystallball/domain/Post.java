package com.katamlek.crystallball.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // let's see how GCP reacts to this strategy
    private Long id;
    // no @Column and so on annotations -- sticking to default to see what GCP needs

    private String postAuthorLogin; // assume no relation user-post although I would maybe suggest the relation; also assume this is well-formed email
    private Date postCreationDate; // no time info as required; I'd rather use LocalDate but it's not allowed with appengine (why?)
    private Date postChangeDate;
    private String postSubject;
    private String postBody;

    // no Hibernate Validator so far as I don't know how it works with GCP

    // annotated with Lombok - no getter, setter, constructor needed
}
