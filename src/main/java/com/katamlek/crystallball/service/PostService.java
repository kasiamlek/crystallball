package com.katamlek.crystallball.service;

import com.katamlek.crystallball.domain.Post;
import com.katamlek.crystallball.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jdo.annotations.Transactional;
import java.util.List;

@Service
public class PostService {
    // there should maybe be a service interface but let's be YAGNI and don't write too much code
    // just for the sake of code

    @Autowired // yes, I could inject in contructor but I like to have it here where I see it
    private PostRepository postRepository;

    // the only methods we need are find all posts and save post (create/update existing)
    // we do not seek by user id, date whatsowever

    @Transactional
    public List<Post> retrieveAllPosts() {
        return postRepository.findAll();
    }

    @Transactional
    public Post saveNewPost(Post post) {
        return postRepository.save(post);
    }

    @Transactional
    public Post saveUpdatedPost(Post post) {
        return postRepository.save(post);
    }

    @Transactional
    public Post findPostById(Long id) {
        return postRepository.findById(id).get();
    }
}
