package com.katamlek.crystallball.controller;

import com.google.api.server.spi.config.*;
import com.katamlek.crystallball.domain.Post;
import com.katamlek.crystallball.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Api(
        name = "echo",
        version = "v1",
        namespace =
        @ApiNamespace(
                ownerDomain = "echo.example.com",
                ownerName = "echo.example.com",
                packagePath = ""
        ),
        // [START_EXCLUDE]
        issuers = {
                @ApiIssuer(
                        name = "firebase",
                        issuer = "https://securetoken.google.com/YOUR-PROJECT-ID",
                        jwksUri =
                                "https://www.googleapis.com/service_accounts/v1/metadata/x509/securetoken@system"
                                        + ".gserviceaccount.com"
                )
        }
// [END_EXCLUDE]
)
@RestController
public class PostController {

    // todo deploy to GCP - seems to work but looks weird, as if it didn't pick the changes?
    // todo Deploy your web application to this project id.
    // todo Test your endpoint via the api explorer ​https://[projectId].appspot.com/_ah/api/explorer

    @Autowired
    private PostService postService;

    @ApiMethod(path = "/posts", httpMethod = ApiMethod.HttpMethod.PUT)
//    @GetMapping("/posts")
    public List<Post> all() {

        // todo User must login, using his google account.
        //  Anyone may have access, but must authenticate before viewing the posts.

        return postService.retrieveAllPosts();
    }

    @PostMapping("/posts")
    public Post newPost(@RequestBody Post newPost) {
        return postService.saveNewPost(newPost);
    }

    @ApiMethod(path = "/posts/{id}", httpMethod = ApiMethod.HttpMethod.PUT)
//    @PutMapping("/posts/{id}")
//    public Post updatePost(@RequestBody Post newPostVersion, @PathVariable Long id) {
    public Post updatePost(@RequestBody Post newPostVersion, @Named("id") Long id) {

        // todo Users may only update their own posts.
        // todo Author of the post is determined from the logged in user.

        Post postToUpdate = postService.saveUpdatedPost(postService.findPostById(id));
        // set properties
        postToUpdate.setPostBody(newPostVersion.getPostBody());
        postToUpdate.setPostSubject(newPostVersion.getPostSubject());
        postToUpdate.setPostChangeDate(new Date());

        return postService.saveUpdatedPost(postService.findPostById(id));
    }
}
