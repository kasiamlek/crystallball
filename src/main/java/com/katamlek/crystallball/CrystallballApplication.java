package com.katamlek.crystallball;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiIssuer;
import com.google.api.server.spi.config.ApiNamespace;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
//@EnableDatastoreRepositories
//@Api(name = "echo", version = "v1", namespace =
//@ApiNamespace(
//        ownerDomain = "echo.example.com",
//        ownerName = "echo.example.com",
//        packagePath = ""
//),
//        issuers = {
//                @ApiIssuer(
//                        name = "firebase",
//                        issuer = "https://securetoken.google.com/YOUR-PROJECT-ID",
//                        jwksUri =
//                                "https://www.googleapis.com/service_accounts/v1/metadata/x509/securetoken@system"
//                                        + ".gserviceaccount.com"
//                )
//        }
//)

public class CrystallballApplication extends SpringBootServletInitializer {

    //todo entire step 5 and 6

    public static void main(String[] args) {
        SpringApplication.run(CrystallballApplication.class, args);
    }

    // this is a dummy controller to test the gcp
//    public class HelloController {
//        @GetMapping("/hello")
//        public String hello() {
//            return "Hello from AppEngine Crystall Ball Welcomes";
//        }
//    }
}
